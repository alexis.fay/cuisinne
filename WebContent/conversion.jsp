<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/fmt' prefix='fmt'%>
<!DOCTYPE html>

<html>
<%@ include file="head.jsp"%>

	<h1 class="center">Conversion des unités de mesures</h1>
	
	<form class="center" method="post">
		<label>Quantité souhaitée : <input type="number" name="number" step="0.001" required></label> 
		<label><select name="unit">
				<c:forEach items="${units}" var="unit">
					<!-- unit = couple de string -->
					<option value="${unit[1]}">${unit[0]}</option>
				</c:forEach>
		</select></label>
		<br><br>
		<button type="submit">Calcul</button>
	</form>
	
	<% if (request.getParameter("number") != null) {%>
		<br>
		
		<table class="center">
			<thead  class="tableau">
				<tr>
					<th class="tableau">Quantité</th>
					<th class="tableau">Unité</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${units}" var="unit" varStatus="status">
					<tr>
						<td class="tableau"><fmt:formatNumber value="${results[status.index]}" maxFractionDigits="3"></fmt:formatNumber></td>
						<td class="tableau">${unit[0]}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	
	<%}%>

<%@ include file="footer.jsp"%>
</body>
</html>